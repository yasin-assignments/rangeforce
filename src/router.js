import Vue from "vue";
import VueRouter from "vue-router";

import BookmarksPage from "@/pages/BookmarksPage.vue";
import DetailsPage from "@/pages/DetailsPage.vue";
import SearchPage from "@/pages/SearchPage.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/bookmarks",
    name: "BookmarksPage",
    component: BookmarksPage
  },
  {
    path: "/details/:id",
    name: "DetailsPage",
    component: DetailsPage
  },
  {
    path: "/",
    name: "SearchPage",
    component: SearchPage
  },
  {
    path: "*",
    redirect: "/"
  }
];

const router = new VueRouter({
  routes
});

export default router;
