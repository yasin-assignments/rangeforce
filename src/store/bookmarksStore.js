export default {
  namespaced: true,

  state: {
    list: JSON.parse(localStorage.getItem("bookmarks")) || {}
  },

  actions: {
    add({ commit }, repository) {
      const aaa = {
        message: `Repository ${repository.full_name} has been added.`,
        revert: () => {
          commit("remove", repository);
          commit("toastStore/setMessage", {}, { root: true });
        }
      };
      commit("toastStore/setMessage", aaa, { root: true });
      commit("add", repository);
    },

    remove({ commit }, repository) {
      const aaa = {
        message: `Repository ${repository.full_name} has been removed.`,
        revert: () => {
          commit("add", repository);
          commit("toastStore/setMessage", {}, { root: true });
        }
      };
      commit("toastStore/setMessage", aaa, { root: true });
      commit("remove", repository);
    }
  },

  mutations: {
    add(state, repository) {
      if (state.list[repository.id]) return;

      const _list = { ...state.list };

      _list[repository.id] = repository;

      state.list = _list;
      localStorage.setItem("bookmarks", JSON.stringify(_list));
    },

    remove(state, repository) {
      if (!state.list[repository.id]) return;

      const _list = { ...state.list };

      delete _list[repository.id];

      state.list = _list;
      localStorage.setItem("bookmarks", JSON.stringify(_list));
    }
  }
};
