import { githubApi } from "./api";

export default {
  namespaced: true,

  state: {
    initialized: false,
    list: [],
    loading: false
  },

  mutations: {
    initialize(state) {
      state.initialized = true;
    },

    setList(state, list) {
      state.list = list;
    },

    setLoading(state, value) {
      state.loading = value;
    }
  },

  actions: {
    async findByKeyword({ commit }, keyword) {
      commit("initialize", true);
      commit("setLoading", true);

      const response = await githubApi.get(`/search/repositories?q=${keyword}`);

      commit("setList", response.data.items);
      commit("setLoading", false);
    }
  }
};
