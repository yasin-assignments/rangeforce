export default {
  namespaced: true,

  state: {
    message: "",
    revert: null,
    interval: null
  },

  mutations: {
    setMessage(state, { message, revert }) {
      state.message = "";
      clearInterval(state.interval);
      state.interval = null;

      state.message = message;
      state.revert = revert;

      state.interval = window.setInterval(() => {
        state.message = "";
        state.revert = revert;
      }, 5000);
    }
  }
};
