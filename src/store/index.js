import Vue from "vue";
import Vuex from "vuex";

import bookmarksStore from "./bookmarksStore";
import githubSearchStore from "./githubSearchStore";
import toastStore from "./toastStore";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    bookmarksStore,
    githubSearchStore,
    toastStore
  }
});
